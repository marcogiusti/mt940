from __future__ import annotations
from abc import ABC, abstractmethod
from enum import Enum
from collections.abc import Iterable
from dataclasses import dataclass
import re
from typing import Dict, List, Tuple


__version__ = "0.2.0"
__all__ = ["compile_fields_specs", "parse"]


class InvalidFieldError(Exception):
    """
    Raised by IFieldParser.parse if it can not parse the sub-field.

    If the sub-field is optional the exception is silently swallowed.
    """


class InvalidFormatError(Exception):
    """
    Raised by parse_format if it can not parse the specifications.
    """


FIELD_RE = re.compile(r"^:(?P<tag>\w+):(?P<data>.*)")
SPEC_FORMAT_RE = re.compile(
    "(?P<length>"
    r"(?:(?P<mm_min>\d+)-(?P<mm_max>\d+))"
    "|"
    r"(?:(?P<exact>\d+)!)"
    "|"
    r"(?:(?P<tm_times>\d+)\*(?P<tm_max>\d+))"
    "|"
    r"(?P<max>\d+)"
    ")"
    "(?P<type>[nacxd])"
)
SWIFT_CHARSET = "[a-zA-Z0-9/?:().,'+{} \r\n-]"
SWIFT_ALPHA = "[a-zA-Z/?:().,'+{} \r\n-]"


class IFieldParser(ABC):

    required: bool = True

    @abstractmethod
    def parse(self, string: str) -> Tuple[str, str]:
        """
        Can raise InvalidFieldError.
        """

        raise NotImplementedError("IFieldParser.parse")


class FieldFormat(str, Enum):
    Numeric = "n"
    Alphabetic = "a"
    AlphaNumeric = "c"
    AnyChar = "x"
    Decimal = "d"

    def __repr__(self):
        return repr(self.value)

    def __str__(self):
        return str(self.value)


@dataclass
class DecimalParser(IFieldParser):
    min_length: int
    max_length: int
    required: bool

    def parse(self, string: str) -> Tuple[str, str]:
        value_buf = []
        for i, char in zip(range(self.max_length), string):
            if char in "0123456789,":
                value_buf.append(char)
            else:
                break
        if len(value_buf) < self.min_length:
            raise InvalidFieldError(string)
        value = "".join(value_buf)
        return value, string[len(value) :]


@dataclass
class RegexParser(IFieldParser):
    regex: re.Pattern[str]
    required: bool

    def parse(self, string: str) -> Tuple[str, str]:
        mo = self.regex.match(string)
        if mo is None:
            raise InvalidFieldError(string)
        else:
            return mo.group("value"), string[mo.end() :]


def parse_format(string_fmt: str) -> IFieldParser:
    required = True
    if string_fmt.startswith("[") and string_fmt.endswith("]"):
        required = False
        string_fmt = string_fmt[1:-1]

    if string_fmt.startswith("regex:"):
        return RegexParser(re.compile(string_fmt[len("regex:") :]), required)
    cleaned_string_fmt = string_fmt.lstrip("/")
    prefix = "/" * (len(string_fmt) - len(cleaned_string_fmt))
    string_fmt = cleaned_string_fmt
    matchobj = SPEC_FORMAT_RE.match(string_fmt)
    if matchobj is None:
        raise InvalidFormatError(string_fmt)
    field_type = matchobj.group("type")
    min_max = matchobj.group("mm_min", "mm_max")
    exact = matchobj.group("exact")
    times_max = matchobj.group("tm_times", "tm_max")
    max = matchobj.group("max")
    if field_type == FieldFormat.Numeric:
        fmt = r"\d"
    elif field_type == FieldFormat.Alphabetic:
        fmt = r"[A-Z]"
    elif field_type == FieldFormat.AlphaNumeric:
        fmt = r"[A-Z0-9]"
    elif field_type == FieldFormat.AnyChar:
        fmt = SWIFT_CHARSET
    elif field_type == FieldFormat.Decimal:
        fmt = None
    else:
        raise InvalidFormatError(string_fmt)

    if fmt is not None:
        if min_max != (None, None):
            fmt = f"{fmt}{{{min_max[0]},{min_max[1]}}}"
        elif exact is not None:
            fmt = f"{fmt}{{{exact}}}"
        elif times_max != (None, None):
            fmt = f"({fmt}{{0,{times_max[1]}}}){{0,{times_max[0]}}}"
        elif max is not None:
            fmt = f"{fmt}{{1,{max}}}"
        return RegexParser(re.compile(f"{prefix}(?P<value>{fmt})"), required)
    else:
        # decimal type
        if min_max != (None, None):
            return DecimalParser(int(min_max[0]), int(min_max[1]), required)
        elif exact is not None:
            return DecimalParser(int(exact), int(exact), required)
        elif max is not None:
            return DecimalParser(1, int(max), required)
        else:
            raise InvalidFormatError(string_fmt)


SubFieldSpecs = Tuple[str, str]
FieldSpecs = Tuple[str, List[SubFieldSpecs], bool]
Specs = Dict[str, FieldSpecs]


def compile_fields_specs(specs: Specs) -> Dict[str, FieldParser]:
    return {tag: FieldParser.compile(tag, spec) for tag, spec in specs.items()}


@dataclass
class SubFieldParser:
    """Sub-field format specification."""

    name: str
    format: IFieldParser
    description: str | None = None

    @classmethod
    def compile(cls, specs: SubFieldSpecs) -> SubFieldParser:
        name, format = specs
        return cls(name, parse_format(format))

    def parse(self, string: str) -> Tuple[SubField, str]:
        value, remaining_string = self.format.parse(string)
        return SubField(self.name, value), remaining_string


@dataclass
class FieldParser:
    """Field format specification."""

    tag: str
    name: str
    subfield_parsers: List[SubFieldParser]
    required: bool = True
    description: str | None = None

    @classmethod
    def compile(cls, tag: str, specs: FieldSpecs) -> FieldParser:
        name, subfield_specs, required = specs
        return cls(
            tag,
            name,
            [SubFieldParser.compile(spec) for spec in subfield_specs],
            required,
        )

    def parse(self, string: str) -> Field:
        remaining_string = string
        subfields = []
        for subfield_parser in self.subfield_parsers:
            try:
                subfield, remaining_string = subfield_parser.parse(remaining_string)
                subfields.append(subfield)
            except InvalidFieldError:
                if subfield_parser.format.required:
                    raise
        return Field(self.tag, self.name, subfields, string)


SubFieldValue = str


@dataclass
class SubField:
    name: str
    value: SubFieldValue


@dataclass
class Field:
    tag: str
    name: str
    subfields: List[SubField]
    raw_data: str

    def get_subfield(self, name: str) -> SubFieldValue:
        for subfield in self.subfields:
            if subfield.name == name:
                return subfield.value
        raise KeyError(name)


@dataclass
class RawField:
    tag: str
    data: str
    start_line: int
    end_line: int

    def parse(self, parser: Dict[str, FieldParser]) -> Field:
        field_parser = parser[self.tag]
        return field_parser.parse(self.data)


def parse(fp: Iterable[str]) -> List[RawField]:
    fileitr = enumerate(iter(fp), start=1)
    field_re = re.compile(FIELD_RE)
    fields = []
    tag = None
    data = []
    start_line = 1
    line_no = 1
    try:
        while True:
            line_no, line = next(fileitr)
            field_match = field_re.match(line)
            if field_match is not None:
                if tag is not None:
                    rf = RawField(tag, "".join(data), start_line, line_no - 1)
                    fields.append(rf)
                tag = field_match.group("tag")
                data = [field_match.group("data")]
                start_line = line_no
            else:
                data.append(line)
    except StopIteration:
        if tag is not None:
            rf = RawField(tag, "".join(data), start_line, line_no)
            fields.append(rf)
    return fields
