============================
Parser for MT940 file format
============================

The library parse the file in different steps.

The first step parses the file in a series of raw fields. These fields
contain just the tag and the raw data.

In a second step these raw fields (``RawField``) can successively parsed
in a series of values.

Quick start
===========

First pass parsing:

.. code:: python

   import mt940

   with open(filename) as fp:
      raw_fields = mt940.parse(fp)


Second pass parsing, here just storing invalid fields:

.. code:: python

   invalid_fields = []
   for raw_field in raw_fields:
      try:
         raw_field.parse(specs)
      except InvalidFieldError as exp:
         invalid_fields.append((raw_field, exp))

External links
==============

- https://en.wikipedia.org/wiki/MT940
- https://www.bankmillennium.pl/documents/10184/128700/File_format_description_of_MT940_v20120309_1216901.pdf/d56bc65f-fa84-4d6c-b07c-9ca60381016b
- https://www.paiementor.com/swift-mt940-format-specifications/
- https://quickstream.westpac.com.au/bankrec-docs/statements/mt940/#mt940-statement-format
- https://www.sepaforcorporates.com/swift-for-corporates/account-statement-mt940-file-format-overview/
