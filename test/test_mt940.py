import json
import re
import unittest

try:
    import yaml
    HAS_YAML = True
except ImportError:
    HAS_YAML = False

import mt940


PARSERS = {
    "20": mt940.FieldParser(
        "20",
        "Transaction Reference Number",
        [
            mt940.SubFieldParser(
                "TRN",
                mt940.RegexParser(
                    re.compile(f"(?P<value>{mt940.SWIFT_CHARSET}{{1,16}})")
                ),
            )
        ],
        True,
    ),
    "21": mt940.FieldParser(
        "21",
        "Related Reference",
        [
            mt940.SubFieldParser(
                "Related Reference",
                mt940.RegexParser(
                    re.compile(f"(?P<value>{mt940.SWIFT_CHARSET}{{1,16}})")
                ),
            ),
        ],
        False,
    ),
    "25": mt940.FieldParser(
        "25",
        "Account Identification",
        [
            mt940.SubFieldParser(
                "SWIFT code",
                mt940.RegexParser(
                    re.compile(f"(?P<value>{mt940.SWIFT_CHARSET}{{1,11}})")
                ),
            ),
            mt940.SubFieldParser(
                "IBAN",
                mt940.RegexParser(
                    re.compile(f"/(?P<value>{mt940.SWIFT_CHARSET}{{1,35}})")
                ),
            ),
        ],
        True,
    ),
    "28C": mt940.FieldParser(
        "28C",
        "Statement Number",
        [
            mt940.SubFieldParser(
                "Statement Number",
                mt940.RegexParser(re.compile(f"(?P<value>\\d{{1,5}})")),
            ),
            mt940.SubFieldParser(
                "Sequence Number",
                mt940.RegexParser(re.compile(f"/(?P<value>\\d{{1,5}})"), False),
            ),
        ],
        True,
    ),
    "60F": mt940.FieldParser(
        "60F",
        "Opening Balance",
        [
            mt940.SubFieldParser(
                "Debit / Credit Mark",
                mt940.RegexParser(re.compile("(?P<value>[A-Z]{1})")),
            ),
            mt940.SubFieldParser(
                "Last / Current Statement Date",
                mt940.RegexParser(re.compile("(?P<value>\\d{6})")),
            ),
            mt940.SubFieldParser(
                "Currency",
                mt940.RegexParser(re.compile("(?P<value>[A-Z]{3})")),
            ),
            mt940.SubFieldParser(
                "Amount",
                mt940.DecimalParser(1, 15),
            ),
        ],
        True,
    ),
    "61": mt940.FieldParser(
        "61",
        "Statement Line",
        [
            mt940.SubFieldParser(
                "Value date",
                mt940.RegexParser(re.compile("(?P<value>\\d{6})")),
            ),
            mt940.SubFieldParser(
                "Entry date",
                mt940.RegexParser(re.compile("(?P<value>\\d{4})"), False),
            ),
            mt940.SubFieldParser(
                "Debit / Credit Mark",
                mt940.RegexParser(re.compile("(?P<value>(?:C|D)R?)")),
                # mt940.RegexParser(re.compile("(?P<value>[A-Z]{1,2})")),
            ),
            mt940.SubFieldParser(
                "Funds Code",
                mt940.RegexParser(re.compile("(?P<value>[A-Z]{1})"), False),
            ),
            mt940.SubFieldParser(
                "Amount",
                mt940.DecimalParser(1, 15),
            ),
            mt940.SubFieldParser(
                "Transaction Type",
                mt940.RegexParser(re.compile("(?P<value>[A-Z]{1})")),
            ),
            mt940.SubFieldParser(
                "ID Code",
                mt940.RegexParser(re.compile("(?P<value>[A-Z0-9]{3})")),
            ),
            mt940.SubFieldParser(
                "Customer Reference",
                mt940.RegexParser(
                    re.compile(f"(?P<value>{mt940.SWIFT_CHARSET}{{1,16}})")
                ),
            ),
            mt940.SubFieldParser(
                "Bank Reference",
                mt940.RegexParser(
                    re.compile(f"//(?P<value>{mt940.SWIFT_CHARSET}{{1,16}})"), False
                ),
            ),
            mt940.SubFieldParser(
                "Supplementary Details",
                mt940.RegexParser(
                    re.compile(f"(?P<value>{mt940.SWIFT_CHARSET}{{1,34}})"), False
                ),
            ),
        ],
        True,
    ),
}
SPECS = {
    "20": ("Transaction Reference Number", [("TRN", "16x")], True),
    "21": ("Related Reference", [("Related Reference", "16x")], False),
    "25": ("Account Identification", [("SWIFT code", "11x"), ("IBAN", "/35x")], True),
    "28C": (
        "Statement Number",
        [("Statement Number", "5n"), ("Sequence Number", "[/5n]")],
        True,
    ),
    "60F": (
        "Opening Balance",
        [
            ("Debit / Credit Mark", "1!a"),
            ("Last / Current Statement Date", "6!n"),
            ("Currency", "3!a"),
            ("Amount", "15d"),
        ],
        True,
    ),
    "61": (
        "Statement Line",
        [
            ("Value date", "6!n"),
            ("Entry date", "[4!n]"),
            # ("Debit / Credit Mark", "2a"),
            ("Debit / Credit Mark", "regex:(?P<value>(?:C|D)R?)"),
            ("Funds Code", "[1!a]"),
            ("Amount", "15d"),
            ("Transaction Type", "1!a"),
            ("ID Code", "3!c"),
            ("Customer Reference", "16x"),
            ("Bank Reference", "[//16x]"),
            # ("Supplementary Details", "CrLf[34x]"),
            ("Supplementary Details", "[34x]"),
        ],
        True,
    ),
}
SPECS_JSON = """\
{
    "20": ["Transaction Reference Number", [["TRN", "16x"]], true],
    "21": ["Related Reference", [["Related Reference", "16x"]], false],
    "25": ["Account Identification", [["SWIFT code", "11x"], ["IBAN", "/35x"]], true],
    "28C": [
        "Statement Number",
        [["Statement Number", "5n"], ["Sequence Number", "[/5n]"]],
        true
    ],
    "60F": [
        "Opening Balance",
        [
            ["Debit / Credit Mark", "1!a"],
            ["Last / Current Statement Date", "6!n"],
            ["Currency", "3!a"],
            ["Amount", "15d"]
        ],
        true
    ],
    "61": [
        "Statement Line",
        [
            ["Value date", "6!n"],
            ["Entry date", "[4!n]"],
            ["Debit / Credit Mark", "regex:(?P<value>(?:C|D)R?)"],
            ["Funds Code", "[1!a]"],
            ["Amount", "15d"],
            ["Transaction Type", "1!a"],
            ["ID Code", "3!c"],
            ["Customer Reference", "16x"],
            ["Bank Reference", "[//16x]"],
            ["Supplementary Details", "[34x]"]
        ],
        true
    ]
}
"""
SPECS_YAML = """\
"20":
    - "Transaction Reference Number"
    - - - "TRN"
        - "16x"
    - true
"21":
    - "Related Reference"
    - - - "Related Reference"
        - "16x"
    - false
"25":
    - "Account Identification"
    - - - "SWIFT code"
        - "11x"
      - - "IBAN"
        - "/35x"
    - true
"28C":
    - "Statement Number"
    - - - "Statement Number"
        - "5n"
      - - "Sequence Number"
        - "[/5n]"
    - true
"60F":
    - "Opening Balance"
    - - - "Debit / Credit Mark"
        - "1!a"
      - - "Last / Current Statement Date"
        - "6!n"
      - - "Currency"
        - "3!a"
      - - "Amount"
        - "15d"
    - true
"61":
    - "Statement Line"
    - - - "Value date"
        - "6!n"
      - - "Entry date"
        - "[4!n]"
      - - "Debit / Credit Mark"
        - "regex:(?P<value>(?:C|D)R?)"
      - - "Funds Code"
        - "[1!a]"
      - - "Amount"
        - "15d"
      - - "Transaction Type"
        - "1!a"
      - - "ID Code"
        - "3!c"
      - - "Customer Reference"
        - "16x"
      - - "Bank Reference"
        - "[//16x]"
      - - "Supplementary Details"
        - "[34x]"
    - true
"""

HEADER_LINES = [
    ":20:BILMT940                                      \n",
    ":25:BILLLULLXXX/LU123456789123456789              \n",
    ":28C:00011/001                                    \n",
    ":60F:C220101EUR12345,67                           \n",
]
HEADER_LINES_RAW_FIELDS = [
    mt940.RawField("20", "BILMT940                                      ", 1, 1),
    mt940.RawField("25", "BILLLULLXXX/LU123456789123456789              ", 2, 2),
    mt940.RawField("28C", "00011/001                                    ", 3, 3),
    mt940.RawField("60F", "C220101EUR12345,67                           ", 4, 4),
]
EXPECTED_HEADER = [
    mt940.Field(
        "20",
        "Transaction Reference Number",
        [mt940.SubField("TRN", "BILMT940        ")],
        "BILMT940                                      ",
    ),
    mt940.Field(
        "25",
        "Account Identification",
        [
            mt940.SubField("SWIFT code", "BILLLULLXXX"),
            mt940.SubField("IBAN", "LU123456789123456789              "),
        ],
        "BILLLULLXXX/LU123456789123456789              ",
    ),
    mt940.Field(
        "28C",
        "Statement Number",
        [
            mt940.SubField("Statement Number", "00011"),
            mt940.SubField("Sequence Number", "001"),
        ],
        "00011/001                                    ",
    ),
    mt940.Field(
        "60F",
        "Opening Balance",
        [
            mt940.SubField("Debit / Credit Mark", "C"),
            mt940.SubField("Last / Current Statement Date", "220101"),
            mt940.SubField("Currency", "EUR"),
            mt940.SubField("Amount", "12345,67"),
        ],
        "C220101EUR12345,67                           ",
    ),
]


class TestMT940(unittest.TestCase):
    def test_parse(self):
        raw_fields = mt940.parse(HEADER_LINES)
        self.assertEqual(raw_fields, HEADER_LINES_RAW_FIELDS)

    def test_parse_raw_fields(self):
        """
        Parse raw fields using a manually created parsers.

        This is the most flexible way to parse raw fields but also the most
        verbose.
        """

        fields = map(lambda rf: rf.parse(PARSERS), HEADER_LINES_RAW_FIELDS)
        self.assertEqual(list(fields), EXPECTED_HEADER)

    def test_compile_fields_specs(self):
        """
        Compile the specifications in SWIFT-notation into parsers.

        The SWIFT-notation is not unambiguous and I did not really found a
        standard for it, just a description in different Banks specifications.
        """

        parsers = mt940.compile_fields_specs(SPECS)
        self.assertDictEqual(parsers, PARSERS)

    def test_compile_fields_specs_from_json(self):
        """
        Specifications in SWIFT-notation can be written in JSON.
        """

        self.maxDiff = None
        specs = json.loads(SPECS_JSON)
        parsers = mt940.compile_fields_specs(specs)
        self.assertDictEqual(parsers, PARSERS)

    @unittest.skipIf(not HAS_YAML, "PyYAML not installed")
    def test_compile_fields_specs_from_yaml(self):
        """
        Specifications in SWIFT-notation can be written in YAML.
        """

        self.maxDiff = None
        specs = yaml.load(SPECS_YAML, yaml.SafeLoader)
        parsers = mt940.compile_fields_specs(specs)
        self.assertDictEqual(parsers, PARSERS)

    def test_get_sub_field(self):
        field = EXPECTED_HEADER[3]  # Opening Balance
        self.assertEqual(field.get_subfield("Debit / Credit Mark"), "C")
        self.assertEqual(field.get_subfield("Amount"), "12345,67")
        self.assertRaises(KeyError, field.get_subfield, "unknown field")
